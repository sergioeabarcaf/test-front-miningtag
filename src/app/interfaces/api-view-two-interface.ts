export interface ApiViewTwoInterface {
  data: string
  error: string
  success: boolean
}

export interface ContentDataAPIViewTwo {
  hasCopyright: boolean
  number: number
  paragraph: string
}

export interface MetaData {
  letter: string
  count: number
}
