export interface errorResponseApi {
  status: boolean,
  reason?: string
}
