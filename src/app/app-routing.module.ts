import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { ViewOneComponent } from './components/view-one/view-one.component';
import { ViewTwoComponent } from './components/view-two/view-two.component';


const routes: Routes = [
  {path: 'viewOne', component: ViewOneComponent },
  {path: 'viewTwo', component: ViewTwoComponent },
  {path: '', component: HomeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
