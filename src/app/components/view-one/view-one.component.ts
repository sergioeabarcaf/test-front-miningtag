import { Component } from '@angular/core';
import { ViewOneService } from '../../services/view-one.service';
import { errorResponseApi } from '../../interfaces/shared-interfaces';
import { ApiViewOneInterface } from 'src/app/interfaces/api-view-one-interface';

@Component({
  selector: 'app-view-one',
  templateUrl: './view-one.component.html',
  styleUrls: ['./view-one.component.css']
})
export class ViewOneComponent {

  public loadingStatus = false;
  public errorStatus: errorResponseApi = {status: false};
  public metaDataUnsorted: ApiViewOneInterface[] | null = null;
  public sortedValues: string = '';

  constructor(private _viewOneService: ViewOneService) {}

  updateTableValues() {
    this.loadingStatus = true;
    this._viewOneService.getApiValues()
      .then((responseAPI) => {
        this.metaDataUnsorted = this.getMetaData(responseAPI);
        this.sortedValues = this.quickSortValues(responseAPI, 0, responseAPI.length - 1);
        console.log(this.metaDataUnsorted, this.sortedValues);
        this.loadingStatus = false;
      })
      .catch(reasonError => {
        this.errorStatus.reason = reasonError;
        this.errorStatus.status = true
      });
  }

  getColorRow(quantity): string {
    if(quantity >= 2){
      return 'table-success'
    }
    if(quantity === 1){
      return 'table-warning'
    }
    if(quantity < 1){
      return 'table-secondary'
    }
  }

  getMetaData(arrayValues): ApiViewOneInterface[] {
    const metaData = [];
    for (const [index, number] of arrayValues.entries()) {
      const indexMetadata = metaData.findIndex(element => element.number === number);
      if (indexMetadata >= 0) {
        metaData[indexMetadata].position.push(index);
      }
      else {
        metaData.push({ number, position: [index] })
      }
    }
    return metaData;
  }

  swap(items, leftIndex, rightIndex) {
    let temp = items[leftIndex];
    items[leftIndex] = items[rightIndex];
    items[rightIndex] = temp;
  }

  partition(items, left, right) {
    let pivot = items[Math.floor((right + left) / 2)];
    while (left <= right) {
      while (items[left] < pivot) {
        left++;
      }
      while (items[right] > pivot) {
        right--;
      }
      if (left <= right) {
        this.swap(items, left, right);
        left++;
        right--;
      }
    }
    return left;
  }

  quickSortValues(values, left, right): string {
    let index;
    if (values.length > 1) {
      index = this.partition(values, left, right);
      if (left < index - 1) {
        this.quickSortValues(values, left, index - 1);
      }
      if (index < right) {
        this.quickSortValues(values, index, right);
      }
    }
    return values.toString();
  }
}
