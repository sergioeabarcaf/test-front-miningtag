import { Component } from '@angular/core';
import { errorResponseApi } from '../../interfaces/shared-interfaces';
import { ContentDataAPIViewTwo, MetaData } from '../../interfaces/api-view-two-interface';
import { ViewTwoService } from '../../services/view-two.service'


@Component({
  selector: 'app-view-two',
  templateUrl: './view-two.component.html',
  styleUrls: ['./view-two.component.css']
})
export class ViewTwoComponent {

  public loadingStatus = false;
  public errorStatus: errorResponseApi = { status: false };
  public values: [MetaData[], number][] = [];
  public headerTable = [
    'Nº', 'a', 'b', 'c', 'd', 'e', 'f',
    'g', 'h', 'i', 'j', 'k', 'l', 'm',
    'n', 'ñ', 'o', 'p', 'q', 'r', 's',
    't', 'u', 'v', 'w', 'x', 'y', 'z',
    'Suma numericos contenidos'
  ];
  public regexletters = [
    {
      regex: /(a)/g,
      letter: 'a'
    },
    {
      regex: /(b)/g,
      letter: 'b'
    },
    {
      regex: /(c)/g,
      letter: 'c'
    },
    {
      regex: /(d)/g,
      letter: 'd'
    },
    {
      regex: /(e)/g,
      letter: 'e'
    },
    {
      regex: /(f)/g,
      letter: 'f'
    },
    {
      regex: /(g)/g,
      letter: 'g'
    },
    {
      regex: /(h)/g,
      letter: 'h'
    },
    {
      regex: /(i)/g,
      letter: 'i'
    },
    {
      regex: /(j)/g,
      letter: 'j'
    },
    {
      regex: /(k)/g,
      letter: 'k'
    },
    {
      regex: /(l)/g,
      letter: 'l'
    },
    {
      regex: /(m)/g,
      letter: 'm'
    },
    {
      regex: /(n)/g,
      letter: 'n'
    },
    {
      regex: /(ñ)/g,
      letter: 'ñ'
    },
    {
      regex: /(o)/g,
      letter: 'o'
    },
    {
      regex: /(p)/g,
      letter: 'p'
    },
    {
      regex: /(q)/g,
      letter: 'q'
    },
    {
      regex: /(r)/g,
      letter: 'r'
    },
    {
      regex: /(s)/g,
      letter: 's'
    },
    {
      regex: /(t)/g,
      letter: 't'
    },
    {
      regex: /(u)/g,
      letter: 'u'
    },
    {
      regex: /(v)/g,
      letter: 'v'
    },
    {
      regex: /(w)/g,
      letter: 'w'
    },
    {
      regex: /(x)/g,
      letter: 'x'
    },
    {
      regex: /(y)/g,
      letter: 'y'
    },
    {
      regex: /(z)/g,
      letter: 'z'
    }
  ];
  public regexNumber = /\d+/g;


  constructor(private _viewTwoService: ViewTwoService) { }

  updateTableValues() {
    this.loadingStatus = true;
    this._viewTwoService.getApiValues()
      .then(data => {
        this.loadingStatus = false;
        const response = this.getInformationFromAPI(data);
        this.values = response;
      })
      .catch(reasonError => {
        this.errorStatus.reason = reasonError;
        this.errorStatus.status = true
      });
  }

  getCountAppearance(paragraph): MetaData[] {
    let letterMetaData: MetaData[] = [];
    for (const letter of this.regexletters) {
      const searchLetterInParaph = paragraph.match(letter.regex);
      if (searchLetterInParaph) {
        letterMetaData.push({ letter: letter.letter, count: searchLetterInParaph.length });
      } else {
        letterMetaData.push({ letter: letter.letter, count: 0 });
      }
    }
    return letterMetaData;
  }

  getSumNumbers(paragraph): number {
    let total = 0;
    const numbersInParagraph = paragraph.match(this.regexNumber);
    if (numbersInParagraph) {
      numbersInParagraph.forEach(number => {
        total += parseInt(number);
      })
    }
    return total
  }

  getInformationFromAPI(dataFromApi: ContentDataAPIViewTwo[]): [MetaData[], number][] {
    let processedValues: [MetaData[], number][] = [];
    for (const data of dataFromApi) {
      const { paragraph } = data;
      let paragraphLower = paragraph.toLowerCase();
      processedValues.push([this.getCountAppearance(paragraphLower), this.getSumNumbers(paragraphLower)]);
    }
    return processedValues;
  }
}
