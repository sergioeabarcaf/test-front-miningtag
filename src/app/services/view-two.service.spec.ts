import { TestBed } from '@angular/core/testing';

import { ViewTwoService } from './view-two.service';

describe('ViewTwoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ViewTwoService = TestBed.get(ViewTwoService);
    expect(service).toBeTruthy();
  });
});
