import { TestBed } from '@angular/core/testing';

import { ViewOneService } from './view-one.service';

describe('ViewOneService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ViewOneService = TestBed.get(ViewOneService);
    expect(service).toBeTruthy();
  });
});
