import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { ApiViewTwoInterface, ContentDataAPIViewTwo } from '../interfaces/api-view-two-interface';

@Injectable({
  providedIn: 'root'
})
export class ViewTwoService {

  private url = environment.apiViewTwoUrl;

  constructor(private http: HttpClient) { }

  getApiValues(): Promise<ContentDataAPIViewTwo[]> {
    return new Promise((resolve, reject) => {
      this.http.get(this.url).subscribe((response: ApiViewTwoInterface) => {
        if (!response.success) {
          reject(response.error);
        } else {
          const dataResponseFormated = JSON.parse(response.data);
          resolve(dataResponseFormated);
        }
      });
    })
  }
}
