import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { ApiViewOneInterface } from '../interfaces/api-view-one-interface';


@Injectable({
  providedIn: 'root'
})
export class ViewOneService {

  private url = environment.apiViewOneUrl;

  constructor(private http: HttpClient) { }

  getApiValues(): Promise<number[]> {
    return new Promise((resolve, reject) => {
      this.http.get(this.url).subscribe((response: ApiViewOneInterface) => {
        console.log(response);
        if (!response.success) {
          reject(response.error);
        } else {
          resolve(response.data);
        }
      });
    })
  }

}
