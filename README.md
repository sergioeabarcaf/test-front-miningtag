# TestFrontMiningtag

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.29. and use:
* Bootstrap - V4.
* Animate CSS - V4.

## Development server

Perform the following steps:
* Run `npm install`
* Copy `src/environments/environment.template.ts` to `src/environments/environment.ts` and fill the parameters required.
* Run `ng serve`
* Navigate to `http://localhost:4200/`

